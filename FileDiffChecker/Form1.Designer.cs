﻿namespace DistinctFileFinder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnPath1 = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.txtPath1 = new System.Windows.Forms.TextBox();
            this.pbDiff = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pbCopyUpdate = new System.Windows.Forms.ProgressBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label3 = new System.Windows.Forms.Label();
            this.pbDir = new System.Windows.Forms.ProgressBar();
            this.lblDir = new System.Windows.Forms.Label();
            this.cbDirPath1 = new System.Windows.Forms.CheckBox();
            this.lblDiff1 = new System.Windows.Forms.Label();
            this.lblCopyUpdate2 = new System.Windows.Forms.Label();
            this.lblCopyUpdate1 = new System.Windows.Forms.Label();
            this.lblCopyBackup2 = new System.Windows.Forms.Label();
            this.lblCopyBackup1 = new System.Windows.Forms.Label();
            this.cbPatternsAll = new System.Windows.Forms.CheckBox();
            this.lblCopyBackupIteration = new System.Windows.Forms.Label();
            this.lblCopyUpdateIteration = new System.Windows.Forms.Label();
            this.lblDiffIteration = new System.Windows.Forms.Label();
            this.lblDirIteration = new System.Windows.Forms.Label();
            this.txtPatterns = new System.Windows.Forms.TextBox();
            this.lblInfoIteration = new System.Windows.Forms.Label();
            this.lblInfo1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pbInfo = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnPath1
            // 
            this.btnPath1.Location = new System.Drawing.Point(11, 29);
            this.btnPath1.Name = "btnPath1";
            this.btnPath1.Size = new System.Drawing.Size(177, 40);
            this.btnPath1.TabIndex = 0;
            this.btnPath1.Text = "Compare Directories (click again to add more)";
            this.btnPath1.UseVisualStyleBackColor = true;
            this.btnPath1.Click += new System.EventHandler(this.btnPath1_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(1070, 98);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(134, 33);
            this.btnOpen.TabIndex = 2;
            this.btnOpen.Text = "Open Diff Folder";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(11, 98);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(177, 33);
            this.btnRun.TabIndex = 3;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // txtPath1
            // 
            this.txtPath1.Location = new System.Drawing.Point(194, 29);
            this.txtPath1.Multiline = true;
            this.txtPath1.Name = "txtPath1";
            this.txtPath1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPath1.Size = new System.Drawing.Size(933, 61);
            this.txtPath1.TabIndex = 4;
            this.txtPath1.TextChanged += new System.EventHandler(this.txtPath1_TextChanged);
            // 
            // pbDiff
            // 
            this.pbDiff.Location = new System.Drawing.Point(13, 236);
            this.pbDiff.Name = "pbDiff";
            this.pbDiff.Size = new System.Drawing.Size(1191, 23);
            this.pbDiff.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 220);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Get Differences";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 267);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Copy Distinct";
            // 
            // pbCopyUpdate
            // 
            this.pbCopyUpdate.Location = new System.Drawing.Point(13, 283);
            this.pbCopyUpdate.Name = "pbCopyUpdate";
            this.pbCopyUpdate.Size = new System.Drawing.Size(1191, 23);
            this.pbCopyUpdate.TabIndex = 8;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Search Directory:";
            // 
            // pbDir
            // 
            this.pbDir.Location = new System.Drawing.Point(13, 150);
            this.pbDir.Name = "pbDir";
            this.pbDir.Size = new System.Drawing.Size(1191, 23);
            this.pbDir.TabIndex = 10;
            // 
            // lblDir
            // 
            this.lblDir.Location = new System.Drawing.Point(227, 134);
            this.lblDir.Name = "lblDir";
            this.lblDir.Size = new System.Drawing.Size(977, 13);
            this.lblDir.TabIndex = 12;
            // 
            // cbDirPath1
            // 
            this.cbDirPath1.AutoSize = true;
            this.cbDirPath1.Checked = true;
            this.cbDirPath1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDirPath1.Location = new System.Drawing.Point(11, 75);
            this.cbDirPath1.Name = "cbDirPath1";
            this.cbDirPath1.Size = new System.Drawing.Size(89, 17);
            this.cbDirPath1.TabIndex = 24;
            this.cbDirPath1.Text = "check subDir";
            this.cbDirPath1.UseVisualStyleBackColor = true;
            // 
            // lblDiff1
            // 
            this.lblDiff1.Location = new System.Drawing.Point(224, 220);
            this.lblDiff1.Name = "lblDiff1";
            this.lblDiff1.Size = new System.Drawing.Size(980, 13);
            this.lblDiff1.TabIndex = 26;
            // 
            // lblCopyUpdate2
            // 
            this.lblCopyUpdate2.Location = new System.Drawing.Point(650, 267);
            this.lblCopyUpdate2.Name = "lblCopyUpdate2";
            this.lblCopyUpdate2.Size = new System.Drawing.Size(554, 13);
            this.lblCopyUpdate2.TabIndex = 29;
            // 
            // lblCopyUpdate1
            // 
            this.lblCopyUpdate1.Location = new System.Drawing.Point(224, 267);
            this.lblCopyUpdate1.Name = "lblCopyUpdate1";
            this.lblCopyUpdate1.Size = new System.Drawing.Size(420, 13);
            this.lblCopyUpdate1.TabIndex = 28;
            // 
            // lblCopyBackup2
            // 
            this.lblCopyBackup2.Location = new System.Drawing.Point(650, 309);
            this.lblCopyBackup2.Name = "lblCopyBackup2";
            this.lblCopyBackup2.Size = new System.Drawing.Size(554, 13);
            this.lblCopyBackup2.TabIndex = 31;
            // 
            // lblCopyBackup1
            // 
            this.lblCopyBackup1.Location = new System.Drawing.Point(221, 309);
            this.lblCopyBackup1.Name = "lblCopyBackup1";
            this.lblCopyBackup1.Size = new System.Drawing.Size(423, 13);
            this.lblCopyBackup1.TabIndex = 30;
            // 
            // cbPatternsAll
            // 
            this.cbPatternsAll.AutoSize = true;
            this.cbPatternsAll.Location = new System.Drawing.Point(12, 5);
            this.cbPatternsAll.Name = "cbPatternsAll";
            this.cbPatternsAll.Size = new System.Drawing.Size(66, 17);
            this.cbPatternsAll.TabIndex = 35;
            this.cbPatternsAll.Text = "Find any";
            this.cbPatternsAll.UseVisualStyleBackColor = true;
            // 
            // lblCopyBackupIteration
            // 
            this.lblCopyBackupIteration.Location = new System.Drawing.Point(163, 309);
            this.lblCopyBackupIteration.Name = "lblCopyBackupIteration";
            this.lblCopyBackupIteration.Size = new System.Drawing.Size(52, 13);
            this.lblCopyBackupIteration.TabIndex = 36;
            // 
            // lblCopyUpdateIteration
            // 
            this.lblCopyUpdateIteration.Location = new System.Drawing.Point(111, 267);
            this.lblCopyUpdateIteration.Name = "lblCopyUpdateIteration";
            this.lblCopyUpdateIteration.Size = new System.Drawing.Size(102, 13);
            this.lblCopyUpdateIteration.TabIndex = 37;
            this.lblCopyUpdateIteration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDiffIteration
            // 
            this.lblDiffIteration.Location = new System.Drawing.Point(111, 220);
            this.lblDiffIteration.Name = "lblDiffIteration";
            this.lblDiffIteration.Size = new System.Drawing.Size(102, 13);
            this.lblDiffIteration.TabIndex = 38;
            this.lblDiffIteration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDirIteration
            // 
            this.lblDirIteration.Location = new System.Drawing.Point(111, 134);
            this.lblDirIteration.Name = "lblDirIteration";
            this.lblDirIteration.Size = new System.Drawing.Size(102, 13);
            this.lblDirIteration.TabIndex = 39;
            this.lblDirIteration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPatterns
            // 
            this.txtPatterns.Location = new System.Drawing.Point(194, 3);
            this.txtPatterns.Name = "txtPatterns";
            this.txtPatterns.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPatterns.Size = new System.Drawing.Size(1010, 20);
            this.txtPatterns.TabIndex = 40;
            this.txtPatterns.TextChanged += new System.EventHandler(this.txtPatterns_TextChanged);
            // 
            // lblInfoIteration
            // 
            this.lblInfoIteration.Location = new System.Drawing.Point(111, 176);
            this.lblInfoIteration.Name = "lblInfoIteration";
            this.lblInfoIteration.Size = new System.Drawing.Size(102, 13);
            this.lblInfoIteration.TabIndex = 45;
            this.lblInfoIteration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInfo1
            // 
            this.lblInfo1.Location = new System.Drawing.Point(224, 176);
            this.lblInfo1.Name = "lblInfo1";
            this.lblInfo1.Size = new System.Drawing.Size(980, 13);
            this.lblInfo1.TabIndex = 43;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Get File Info";
            // 
            // pbInfo
            // 
            this.pbInfo.Location = new System.Drawing.Point(13, 192);
            this.pbInfo.Name = "pbInfo";
            this.pbInfo.Size = new System.Drawing.Size(1191, 23);
            this.pbInfo.TabIndex = 41;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Firebrick;
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(1133, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 60);
            this.button1.TabIndex = 46;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(142, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 19);
            this.label4.TabIndex = 47;
            this.label4.Text = "Or:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 318);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblInfoIteration);
            this.Controls.Add(this.lblInfo1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pbInfo);
            this.Controls.Add(this.txtPatterns);
            this.Controls.Add(this.lblDirIteration);
            this.Controls.Add(this.lblDiffIteration);
            this.Controls.Add(this.lblCopyUpdateIteration);
            this.Controls.Add(this.lblCopyBackupIteration);
            this.Controls.Add(this.cbPatternsAll);
            this.Controls.Add(this.lblCopyBackup2);
            this.Controls.Add(this.lblCopyBackup1);
            this.Controls.Add(this.lblCopyUpdate2);
            this.Controls.Add(this.lblCopyUpdate1);
            this.Controls.Add(this.lblDiff1);
            this.Controls.Add(this.cbDirPath1);
            this.Controls.Add(this.lblDir);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pbDir);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pbCopyUpdate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbDiff);
            this.Controls.Add(this.txtPath1);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnPath1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Distinct File Finder";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPath1;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.TextBox txtPath1;
        private System.Windows.Forms.ProgressBar pbDiff;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar pbCopyUpdate;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar pbDir;
        private System.Windows.Forms.Label lblDir;
        private System.Windows.Forms.CheckBox cbDirPath1;
        private System.Windows.Forms.Label lblDiff1;
        private System.Windows.Forms.Label lblCopyUpdate2;
        private System.Windows.Forms.Label lblCopyUpdate1;
        private System.Windows.Forms.Label lblCopyBackup2;
        private System.Windows.Forms.Label lblCopyBackup1;
        private System.Windows.Forms.CheckBox cbPatternsAll;
        private System.Windows.Forms.Label lblCopyBackupIteration;
        private System.Windows.Forms.Label lblCopyUpdateIteration;
        private System.Windows.Forms.Label lblDiffIteration;
        private System.Windows.Forms.Label lblDirIteration;
        private System.Windows.Forms.TextBox txtPatterns;
        private System.Windows.Forms.Label lblInfoIteration;
        private System.Windows.Forms.Label lblInfo1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ProgressBar pbInfo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
    }
}

