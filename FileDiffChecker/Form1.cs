﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Security.Cryptography;

namespace DistinctFileFinder
{
    public partial class Form1 : Form
    {
        public int WindowRunHeight { get; set; } = 367;
        public int OriginalWindowHeight { get; set; }

        public class enumFileList
        {
            public string dir { get; set; }
            public string pattern { get; set; }
            public SearchOption option { get; set; }
        }

        public class FileInfo
        {
            public string path { get; set; }
            public string root { get; set; }
            public string fileNameAndPathsPastRoot { get; set; }
            public string hash { get; set; }
            public Boolean readyToCopy { get; set; } = false;
        }

        List<FileInfo> filePathsToCopy { get; set; }
        public string baseSaveDir { get; set; }
        public string[] basePatterns { get; set; }
        //List<enumFileList> dirListToCheck { get; set; }

        public Form1()
        {
            InitializeComponent();
            txtPath1.Text = @"";
            btnRun.Visible = enableRunButton();
            btnOpen.Visible = false;
            filePathsToCopy = new List<FileInfo>();
            baseSaveDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\UploadFolder";
            basePatterns = new[] { "*.js", "*.css", "*.aspx", "*.dll" };
            txtPatterns.Text = basePatterns.ToCsv();
        }

        private string GetPath()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    return fbd.SelectedPath;
                }
            }
            return "";
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy != true)
            {
                this.Height = WindowRunHeight;
                btnRun.Enabled = false;
                txtPatterns.Enabled = false;
                btnPath1.Enabled = false;
                button1.Enabled = false;
                txtPath1.Enabled = false;
                // Start the asynchronous operation.
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private string createDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                return path;
            }
            else
            {
                return createDirectory(path + "_Copy");
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", baseSaveDir);
        }

        public static DateTime? GetLastWriteTime(string path)
        {
            if (File.Exists(path))
            {
                return File.GetLastWriteTime(path);
            }
            return null;
        }

        private bool enableRunButton()
        {
            return txtPath1.Text != "";
        }

        private void btnPath1_Click(object sender, EventArgs e)
        {
            List<string> oldText = new List<string>();
            //add old text from screen
            if (txtPath1.Text != "")
            {
                oldText = txtPath1.Text.ToCsv().ToList();
            }
                        
            var dialog = new CommonOpenFileDialog
            {
                IsFolderPicker = true,
                Multiselect = true,
                InitialDirectory = txtPath1.Text.ToCsv().ToList().FindRoot()
            };
            CommonFileDialogResult result = dialog.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                //add new selections
                oldText.AddRange(dialog.FileNames);
            }

            //Add all back
            txtPath1.Text = oldText.ToList().ToCsv().ToNewLine();
            btnRun.Visible = enableRunButton();
        }

        private void txtPath2_TextChanged(object sender, EventArgs e)
        {
            btnRun.Visible = enableRunButton();
        }

        private void txtPath1_TextChanged(object sender, EventArgs e)
        {
            btnRun.Visible = enableRunButton();
        }

        // Takes same patterns, and executes in parallel
        private IEnumerable<string> GetFiles(string path, string[] searchPatterns, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            return searchPatterns.AsParallel()
                .SelectMany(searchPattern =>
                    Directory.EnumerateFiles(path, searchPattern, searchOption));
        }

        private string[] getPatterns()
        {
            return cbPatternsAll.Checked == false ? basePatterns : new[] { "*.*" };
        }

        string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.WorkerReportsProgress = true;
            var fCounter = 0;

            //dirs
            baseSaveDir = createDirectory(baseSaveDir);
            var saveUpdatePath = baseSaveDir + @"\Updates";

            //path 1
            var path1List = txtPath1.Text.ToCsv().ToList();
            worker.ReportProgress(path1List.Count, "pbDir_Maximum");
            worker.ReportProgress(1, "pbDir_PerformStep");
            var p1Root = path1List.FindRoot();
            List<string> path1files = new List<string>();
            fCounter = 0;
            worker.ReportProgress(fCounter, "pbDir_Iteration_" + path1List.Count);
            foreach (var path in path1List)
            {
                worker.ReportProgress(++fCounter, "pbDir_Iteration_" + path1List.Count);
                worker.ReportProgress(1, "pbDir_path_" + path);
                worker.ReportProgress(1, "pbDir_PerformStep");
                path1files.AddRange(GetFiles(path, getPatterns(), cbDirPath1.Checked ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
            }

            //get file's info
            fCounter = 0;
            worker.ReportProgress(path1files.Count, "pbInfo_Maximum");
            worker.ReportProgress(1, "pbInfo_PerformStep");
            var p1InfoList = new List<FileInfo>();
            foreach (var p1 in path1files)
            {
                worker.ReportProgress(++fCounter, "pbInfo_Iteration_" + path1files.Count);
                worker.ReportProgress(1, "pbInfo_1_" + p1);
                worker.ReportProgress(1, "pbInfo_PerformStep");

                //var file1 = new FileInfo(p1);
                var p1FileNameWithStructure = p1.Replace(p1Root, "");
                var p1Info = new FileInfo
                {
                    path = p1,
                    fileNameAndPathsPastRoot = p1FileNameWithStructure,
                    readyToCopy = false,
                    root = p1Root,
                    hash = CalculateMD5(p1)
                };

                p1InfoList.Add(p1Info);
            }

            //see if they should be copied
            fCounter = 0;
            worker.ReportProgress(fCounter, "pbDiff_Iteration_" + path1files.Count);
            foreach (var p1 in p1InfoList)
            {
                worker.ReportProgress(++fCounter, "pbDiff_Iteration_" + path1files.Count);
                worker.ReportProgress(1, "pbDiff_1_" + p1);
                worker.ReportProgress(1, "pbDiff_PerformStep");

                if (!filePathsToCopy.Any(w => w.hash == p1.hash))
                {
                    filePathsToCopy.Add(p1);
                }
            }

            worker.ReportProgress(1, "pbDiff_1_" + "");

            //save saveUpdatePath files
            worker.ReportProgress(filePathsToCopy.Count, "pbCopy_UPDATE_Maximum");
            fCounter = 0;
            worker.ReportProgress(fCounter, "pbCopy_UPDATE_Iteration_" + filePathsToCopy.Count);
            foreach (var f in filePathsToCopy)
            {
                worker.ReportProgress(++fCounter, "pbCopy_UPDATE_Iteration_" + filePathsToCopy.Count);
                worker.ReportProgress(1, "pbCopy_UPDATE_1_" + f);
                var fileSavePath = saveUpdatePath + @"\" + f.path.Replace(p1Root, "");
                worker.ReportProgress(1, "pbCopy_UPDATE_2_" + fileSavePath);
                if (File.Exists(fileSavePath))
                {
                    File.Delete(fileSavePath);
                }
                var dir = fileSavePath.Replace(Path.GetFileName(f.path), "");
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                File.Copy(f.path, fileSavePath);
                worker.ReportProgress(1, "pbCopy_UPDATE_PerformStep");
            }
            worker.ReportProgress(1, "pbCopy_UPDATE_1_" + "");
            worker.ReportProgress(1, "pbCopy_UPDATE_2_" + "");
        }

        private void setMax(ProgressBar bar, int max)
        {
            bar.Value = 0;
            bar.Step = 1;
            bar.Maximum = max;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (e.UserState)
            {
                case "pbDir_Maximum":
                    setMax(pbDir, e.ProgressPercentage);
                    pbDiff.Value = 0;
                    break;
                case "pbInfo_Maximum":
                    setMax(pbInfo, e.ProgressPercentage);
                    pbInfo.Value = 0;
                    break;
                case "pbDiff_Maximum":
                    setMax(pbDiff, e.ProgressPercentage);
                    break;
                case "pbCopy_UPDATE_Maximum":
                    setMax(pbCopyUpdate, e.ProgressPercentage);
                    break;
                case "pbDir_PerformStep":
                    pbDir.PerformStep();
                    break;
                case "pbInfo_PerformStep":
                    pbInfo.PerformStep();
                    break;
                case "pbDiff_PerformStep":
                    pbDiff.PerformStep();
                    break;
                case "pbCopy_UPDATE_PerformStep":
                    pbCopyUpdate.PerformStep();
                    break;
            }
            if (e.UserState.ToString().Contains("pbDir_path_"))
            {
                lblDir.Text = e.UserState.ToString().Replace("pbDir_path_", "");
            }
            if (e.UserState.ToString().Contains("pbDiff_1_"))
            {
                lblDiff1.Text = e.UserState.ToString().Replace("pbDiff_1_", "");
            }
            if (e.UserState.ToString().Contains("pbInfo_1_"))
            {
                lblInfo1.Text = e.UserState.ToString().Replace("pbInfo_1_", "");
            }
            if (e.UserState.ToString().Contains("pbCopy_UPDATE_1_"))
            {
                lblCopyUpdate1.Text = e.UserState.ToString().Replace("pbCopy_UPDATE_1_", "");
            }
            if (e.UserState.ToString().Contains("pbCopy_UPDATE_2_"))
            {
                lblCopyUpdate1.Text = e.UserState.ToString().Replace("pbCopy_UPDATE_2_", "");
            }
            if (e.UserState.ToString().Contains("pbCopy_UPDATE_Iteration_"))
            {
                lblCopyUpdateIteration.Text = e.ProgressPercentage + @"/" + e.UserState.ToString().Replace("pbCopy_UPDATE_Iteration_", "");
            }
            if (e.UserState.ToString().Contains("pbDir_Iteration_"))
            {
                lblDirIteration.Text = e.ProgressPercentage + @"/" + e.UserState.ToString().Replace("pbDir_Iteration_", "");
            }
            if (e.UserState.ToString().Contains("pbDiff_Iteration_"))
            {
                lblDiffIteration.Text = e.ProgressPercentage + @"/" + e.UserState.ToString().Replace("pbDiff_Iteration_", "");
            }
            if (e.UserState.ToString().Contains("pbInfo_Iteration_"))
            {
                lblInfoIteration.Text = e.ProgressPercentage + @"/" + e.UserState.ToString().Replace("pbInfo_Iteration_", "");
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnOpen.Visible = true;

            //string startPath = baseSaveDir + @"\Updates";
            //string zipPath = baseSaveDir + @"\updates.zip";
            //try
            //{
            //    ZipFile.CreateFromDirectory(startPath, zipPath);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}

            ////ZipFile.ExtractToDirectory(zipPath, extractPath);

            this.Height = OriginalWindowHeight;
            MessageBox.Show("Aaaaaaand we're done");
            Process.Start("explorer.exe", baseSaveDir);

            btnRun.Enabled = true;
            button1.Enabled = true;
            txtPatterns.Enabled = true;
            btnPath1.Enabled = true;
            txtPath1.Enabled = true;
            btnOpen.Visible = true;

            this.Close();
        }

        private void txtPatterns_TextChanged(object sender, EventArgs e)
        {
            basePatterns = txtPatterns.Text.ToArray();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtPath1.Text = "";
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            OriginalWindowHeight = this.Height;
        }
    }
}
