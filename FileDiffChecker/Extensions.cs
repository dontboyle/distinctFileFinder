﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DistinctFileFinder
{
    public static class Extensions
    {
        /// <summary>
        /// join list to array , 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string ToCsv(this List<string> list)
        {
            return string.Join(",", list);
        }
        /// <summary>
        /// join string[] to array ,
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static string ToCsv(this string[] arr)
        {
            return string.Join(",", arr);
        }
        /// <summary>
        /// split csv into list
        /// </summary>
        /// <param name="csv"></param>
        /// <returns></returns>
        public static List<string> ToList(this string csv)
        {
            return csv.Split(',').ToList();
        }
        /// <summary>
        /// turns list into array
        /// </summary>
        /// <param name="csv"></param>
        /// <returns></returns>
        public static string[] ToArray(this string csv)
        {
            return csv.Split(',').Select(i=>i.ToString()).ToArray();
        }
        /// <summary>
        /// replace , with new line
        /// </summary>
        /// <param name="csv"></param>
        /// <returns></returns>
        public static string ToNewLine(this string csv)
        {
            return csv.Replace(",", Environment.NewLine);
        }
        /// <summary>
        /// replace newline with ,
        /// </summary>
        /// <param name="newLine"></param>
        /// <returns></returns>
        public static string ToCsv(this string newLine)
        {
            return newLine.Replace(Environment.NewLine, ",");
        }
        /// <summary>
        /// get longest common prefix
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetLongestCommonPrefix(string[] s)
        {
            int k = s[0].Length;
            for (int i = 1; i < s.Length; i++)
            {
                k = Math.Min(k, s[i].Length);
                for (int j = 0; j < k; j++)
                    if (s[i][j] != s[0][j])
                    {
                        k = j;
                        break;
                    }
            }
            return s[0].Substring(0, k);
        }
        /// <summary>
        /// removes last path 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string CutToEndPath(this string s)
        {
            var index = s.LastIndexOf(@"\");
            s = s.Substring(0, index);
            return s;
        }
        /// <summary>
        /// finds root of paths if single path this just returns path
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static string FindRoot(this List<string> paths)
        {
            List<string> sortedPaths = paths.OrderBy(s => s).ToList();
            if (sortedPaths[0] == sortedPaths[sortedPaths.Count - 1])
            {
                return sortedPaths[0];
            }
            else
            {
                return GetLongestCommonPrefix(new string[] { sortedPaths[0], sortedPaths[sortedPaths.Count - 1] }).CutToEndPath();
            }
        }
        /// <summary>
        /// comparing file bytes
        /// </summary>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int memcmp(byte[] b1, byte[] b2, long count);
        public static bool Compare(this byte[] b1, byte[] b2)
        {
            // Validate buffers are the same length.
            // This also ensures that the count does not exceed the length of either buffer.  
            return b1.Length == b2.Length && memcmp(b1, b2, b1.Length) == 0;
        }
    }
}
